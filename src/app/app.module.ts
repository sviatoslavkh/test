import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { EditorModule } from './editor/editor.module';
import { TextService } from './services/text-service/text.service';
import { FormattingService } from './services/formatting-service/formatting.service';
import { SynonymsService } from './services/synonyms-service/synonyms.service';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    EditorModule
  ],
  providers: [
    TextService,
    FormattingService,
    SynonymsService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
