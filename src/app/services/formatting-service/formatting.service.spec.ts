/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FormattingService } from './formatting.service';

describe('FormattingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormattingService]
    });
  });

  it('should ...', inject([FormattingService], (service: FormattingService) => {
    expect(service).toBeTruthy();
  }));
});
