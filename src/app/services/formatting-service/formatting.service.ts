import { Injectable } from '@angular/core';

interface IFormattingRules {
  [id: string]: string;
}

@Injectable()
export class FormattingService {
  private formattingRules: IFormattingRules = {
    'bold': 'formatting--bold',
    'italic': 'formatting--italic',
    'underline': 'formatting--underline',
  };

  public getFormattingStyles(id: string): string {
    return this.formattingRules[id];
  }
}
