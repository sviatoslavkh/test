import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import _words from "lodash/words";
import { Observable } from "rxjs";

export interface IApiResponse {
    word: string;
    score: number;
    tags: Array<string>;
}

@Injectable()
export class SynonymsService {
  private synonymsResource = 'https://api.datamuse.com';

  constructor(private http: HttpClient) { }

  public getSynonyms(word: string): Observable<Array<IApiResponse>> {
    return this.http.get(`${this.synonymsResource}/words?ml=${_words(word)}`) as Observable<Array<IApiResponse>>;
  }
}
