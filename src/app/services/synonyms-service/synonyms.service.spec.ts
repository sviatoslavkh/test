/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SynonymsService } from './synonyms.service';

describe('SynonymsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SynonymService]
    });
  });

  it('should ...', inject([SynonymService], (service: SynonymService) => {
    expect(service).toBeTruthy();
  }));
});
