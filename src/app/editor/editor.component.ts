import { ChangeDetectionStrategy, Component, HostListener, ViewChild } from '@angular/core';
import { IWord } from './file/file.component';
import { SynonymsService, IApiResponse } from '../services/synonyms-service/synonyms.service';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import _words from "lodash/words";
import _replace from "lodash/replace";

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditorComponent {
  private selectedWord: IWord;

  public synonyms$: Observable<Array<IApiResponse>>;
  // Every click should reset the selected word
  @HostListener('document:click', ['$event'])
    hostClick(event: MouseEvent) {
        this.selectedWord = undefined;
    }

  constructor(private synonymsService: SynonymsService) { }

  public formattingChanged(formattingRuleId: string) {
    if (this.selectedWord) {
      if (this.selectedWord.formattingRulesIds) {
        const formattingRules = this.selectedWord.formattingRulesIds;
        this.toggleformattingRule(formattingRules, formattingRuleId);
      } else {
        this.selectedWord.formattingRulesIds = [formattingRuleId];
      }
    }
  }

  public wordSelected(word: IWord) {
    this.selectedWord = word;
  }

  public synonymSelected(synonym: string) {
    if (this.selectedWord) {
      const wordToReplace = _words(this.selectedWord.wordString)[0];
      this.selectedWord.wordString = _replace(this.selectedWord.wordString, wordToReplace, synonym);
    }
  }

  public synonymsRequested() {
    if (this.selectedWord) {
      this.synonyms$ = this.synonymsService.getSynonyms(this.selectedWord.wordString).pipe(map(response => response.slice(0, 10)));
    }
  }

  private toggleformattingRule(formattingRules: Array<string>, formattingRuleId: string) {
    const ruleIndexInWord = formattingRules.indexOf(formattingRuleId);
    if (ruleIndexInWord !== -1) {
      formattingRules.splice(ruleIndexInWord, 1);
    } else {
      formattingRules.push(formattingRuleId);
    }
  }
}
