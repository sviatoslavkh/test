import { ChangeDetectionStrategy, Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';
import { TextService } from '../../services/text-service/text.service';
import { FormattingService } from "../../services/formatting-service/formatting.service";

export interface IWord {
    index: number;
    wordString: string;
    formattingRulesIds: Array<string>;
}

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss']
})
export class FileComponent implements OnInit {
  public text: Array<IWord>;
  @Input() formattingRules = [];

  @Output() wordSelected = new EventEmitter<IWord>();

  constructor(private textService: TextService,
              private formattingService: FormattingService,
              private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.textService.getMockText().subscribe(text => {
       this.text = this.transformTextIntoWords(text).map((word, index) => ({
         index,
         wordString: word,
         formattingRulesIds: undefined
       }));
    });
  }

  public selectWord(word: IWord): void {
    this.wordSelected.emit(word);
  }

  public getStyles(formattingRulesIds: Array<string>) {
    if (!formattingRulesIds) {
      return '';
    }

    const classes = formattingRulesIds.reduce((acc, currentRule) => {
      const formattingClass = this.formattingService.getFormattingStyles(currentRule);
      return `${acc} ${formattingClass}`;
    }, '');
    return classes;
  }

  private transformTextIntoWords(text: string): Array<string> {
    return text.split(' ');
  }
}
