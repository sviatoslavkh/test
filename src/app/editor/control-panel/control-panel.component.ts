import { ChangeDetectionStrategy, Component, Output, EventEmitter, Input } from '@angular/core';
import { IApiResponse } from '../../services/synonyms-service/synonyms.service';
import { Observable } from 'rxjs';
import { IWord } from '../file/file.component';

export interface IFormattingControl {
  id: string;
  label: string;
  class: string;
}

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss'],
  host: {class: "control-panel"},
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlPanelComponent {
  @Input() appliedFormattingRules: Array<string> = [];
  @Input() synonyms: Observable<Array<IApiResponse>>;
  @Input() selectedWord: IWord;

  public controls: Array<IFormattingControl> = [{
    id: 'bold',
    label: `B`,
    class: 'control-panel__control--bold'
  }, {
    id: 'italic',
    label: `I`,
    class: 'control-panel__control--italic'
  }, {
    id: 'underline',
    label: `U`,
    class: 'control-panel__control--underline'
  }];

  @Output() formattingChanged = new EventEmitter<string>();
  @Output() synonymsRequested = new EventEmitter<void>();
  @Output() synonymSelected = new EventEmitter<string>();

  public selectFormatter(event: MouseEvent, formatterId: string) {
    event.stopPropagation();
    this.formattingChanged.emit(formatterId);
  }

  public isApplied(formattingRuleId: string) {
    return this.appliedFormattingRules ?
           this.appliedFormattingRules.indexOf(formattingRuleId) !== -1 :
           false;
  }

  public requestSynonyms(event: MouseEvent) {
    event.stopPropagation();
    this.synonymsRequested.emit();
  }

  public selectSynonym(word: string) {
    this.synonymSelected.emit(word);
  }
}
