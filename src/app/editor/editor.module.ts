import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { FileComponent } from './file/file.component';
import { ControlPanelComponent } from './control-panel/control-panel.component';
import { EditorComponent } from './editor.component';

@NgModule({
  declarations: [
    FileComponent,
    ControlPanelComponent,
    EditorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbDropdownModule
  ],
  exports: [
    EditorComponent
  ]
})
export class EditorModule {
}
